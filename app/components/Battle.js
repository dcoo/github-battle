var React = require('react');
var PropTypes = require('prop-types');
var Link = require('react-router-dom').Link;
var PlayerPreview = require('./PlayerPreview')

class PlayerInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (event) {
        var value = event.target.value; // text inside of submit

        this.setState(function () {
            return {
                username: value
            }
        });
    }

    handleSubmit (event) {
        event.preventDefault();

        this.props.onSubmit (
            this.props.id,
            this.state.username
        )
    }

    render () {
        // this.props.label and this.props.id are passed in form Battle render() method
        return (
            <form className='column' onSubmit={this.handleSubmit}>
                <label className='header' htmlFor='username'>
                    {this.props.label}
                </label>
                <input 
                    id='username' 
                    placeholder='github username' 
                    type='text' 
                    autoComplete='off'
                    value={this.state.username}
                    onChange={this.handleChange}
                >
                </input>
                <button 
                    className= 'button'
                    type= 'submit'
                    disabled = {!this.state.username}>
                    Submit
                </button>
            </form>
        )
    }
}

PlayerInput.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired
}


class Battle extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            p1Name: '',
            p2Name: '',
            p1Img: null,
            p2Img: null,
        };

        // because we call 'this' within handleSubmit, we need to bind it to the scope of this outside of the function
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleSubmit (id, username) {
        this.setState(function () {
            var newState = {};
            newState[id + 'Name'] = username;
            newState[id + 'Img'] = 'https://github.com/' +  username + '.png?size=200';
            return newState;
        });
    }

    handleReset (id) {
        this.setState(function () {
            var newState = {};
            newState[id + 'Name'] = '';
            newState[id + 'Img'] = null;
            return newState;
        });
    }

    render() {
        var match = this.props.match;
        var p1Name = this.state.p1Name;
        var p2Name = this.state.p2Name;
        var p1Img = this.state.p1Img;
        var p2Img = this.state.p2Img;

        return (
            <div>
                <div className='row'>
                    {!p1Name && <PlayerInput 
                        id='p1'
                        label='Player One'
                        onSubmit={this.handleSubmit}
                    />}

                    { p1Img !== null && 
                    <PlayerPreview 
                        avatar = {p1Img}
                        username = {p1Name}>
                        <button
                            className='reset'
                            onClick={this.handleReset.bind(null,'p1')}>
                                Reset
                        </button>
                    </PlayerPreview>}

                    {!p2Name && <PlayerInput 
                        id='p2'
                        label='Player 2'
                        onSubmit={this.handleSubmit}
                    />}

                    {p2Img !== null && 
                    <PlayerPreview 
                        avatar = {p2Img}
                        username = {p2Name}>
                        <button
                            className='reset'
                            onClick={this.handleReset.bind(null,'p2')}>
                                Reset
                        </button>
                    </PlayerPreview>}

                </div>

                {p1Img && p2Img && 
                    <Link 
                        className='button'
                        to={{
                            pathname: match.url + '/results',
                            search: `?p1Name=` + p1Name + `&p2Name=` + p2Name
                        }}>
                        Battle
                    </Link>
                }

            </div>
        )
    }
}

module.exports = Battle;